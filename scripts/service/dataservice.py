from flask import request, Blueprint
from scripts.handler.datahandler import data_parsing

data = Blueprint("Data", __name__)


@data.route('/post', methods=['POST'])
def postjsonhandler():
    content = request.get_json()
    data_parsing(content)
    return str(content)
