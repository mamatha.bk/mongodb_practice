from pymongo import MongoClient


def data_parsing(data):
    client = MongoClient('localhost', 27017)
    db = client["mamatha"]
    Collection = db["user_details_2"]
    if isinstance(data, list):
        Collection.insert_many(data)
        print("Inserted")
    else:
        Collection.insert_one(data)
        print("Inserted")

    #using find()

    for data in Collection.find({}, {"gender": 1}):
        print(data)
        #Using find_one()
    one = Collection.find_one()
    print(one)
    #Using update()
    myquery = {"gender": "Male"}
    newvalues = {"$set": {"gender": "male"}}

    Collection.update_many(myquery, newvalues)
    for data in Collection.find({},{"gender":1}):
        print(data)
